var slideIndex = -1;
showSlidesAuto();

// Next/previous controls
function plusSlides(n) {
  showSlides(slideIndex += n);
}

// Thumbnail image controls
function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("dot");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
}

function showSlidesAuto() {
  showSlides(slideIndex += 1);
  setTimeout(showSlidesAuto, 5000); // Change image every 5 seconds
}

/*
var http = require("https");

var options = {
	"method": "GET",
	"hostname": "nutritionix-api.p.rapidapi.com",
	"port": null,
	"path": "/v1_1/search/cheddar%2520cheese?fields=nf_serving_size_unit%252Cnf_serving_size_qty%252Citem_name%252Citem_id%252Cbrand_name%252Cnf_calories%252Cnf_total_fat",
	"headers": {
		"x-rapidapi-host": "nutritionix-api.p.rapidapi.com",
		"x-rapidapi-key": "473f810198msh07f275b3d65dd55p17f3c3jsn02aef6ecc252"
	}
};

var req = http.request(options, function (res) {
	var chunks = [];

	res.on("data", function (chunk) {
		chunks.push(chunk);
	});

	res.on("end", function () {
		var body = Buffer.concat(chunks);
		console.log(body.toString());
	});
});

req.end();*/